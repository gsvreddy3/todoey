//
//  CategoryViewController.swift
//  Todoey
//
//  Created by sai varun reddy Gopireddy on 25/11/18.
//  Copyright © 2018 sai varun reddy Gopireddy. All rights reserved.
//

import UIKit
import CoreData
import RealmSwift

class CategoryViewController: UITableViewController {
    
    let realm = try! Realm()
    var categoryArray : Results<Category>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCategories()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryArray?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath)
        
        cell.textLabel?.text = categoryArray?[indexPath.row].name ?? "Start Adding a Category!"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToItems", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! TodoListViewController
        if let indexpath = tableView.indexPathForSelectedRow {
            destinationVC.selectedCategory = categoryArray?[indexpath.row]
        }
    }
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        var textField = UITextField()
        
        let alert = UIAlertController(title: "Add a new Category!", message: "", preferredStyle: .alert)
        let alertaction = UIAlertAction(title: "Add Category", style: .default) { (alertaction) in
            let category = Category()
            category.name = textField.text!
            self.saveCategories(category)
        }
        
        alert.addTextField { (uiTextField) in
            uiTextField.placeholder = "Create a new Category"
            textField = uiTextField
        }
        
        alert.addAction(alertaction)
        
        present(alert, animated: true, completion: nil)
        
        
    }
    
    func saveCategories(_ category : Category){
        do{
            try realm.write {
                realm.add(category)
            }
        }
        catch{
            print("Error in saving \(error)")
        }
        tableView.reloadData()
    }
    
    func loadCategories(){
        categoryArray = realm.objects(Category.self)
        tableView.reloadData()
    }

}
