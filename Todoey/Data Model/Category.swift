//
//  Category.swift
//  Todoey
//
//  Created by sai varun reddy Gopireddy on 28/11/18.
//  Copyright © 2018 sai varun reddy Gopireddy. All rights reserved.
//

import Foundation
import RealmSwift

class Category : Object{
    @objc dynamic var name : String = ""
    let items = List<Item>()
}
