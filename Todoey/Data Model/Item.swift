//
//  Item.swift
//  Todoey
//
//  Created by sai varun reddy Gopireddy on 28/11/18.
//  Copyright © 2018 sai varun reddy Gopireddy. All rights reserved.
//

import Foundation
import RealmSwift

class Item: Object {
    @objc dynamic var title : String = ""
    @objc dynamic var done : Bool = false
    var parentCategory = LinkingObjects(fromType: Category.self, property: "items")
}
